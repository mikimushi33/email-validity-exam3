 #include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

bool specialchars (char*emailaddress)
{   if (!emailaddress){
        printf("No Email Address inside.\n");
        return 0; }
    int at=0;
    int dot=0;
    int i;
    for(i=0;emailaddress[i]!='\0';i++)
    {
        if(emailaddress[i]=='@') at++;
        if(emailaddress[i]=='.') dot++;
    }
    if(dot<1) printf("The email address has to contain at least one dot in it.\n");
    if(at<1||at>1) printf("The email address has to contain only one '@' character.\n");
    if(dot>0&&at==1) return true;
    else return false;
}
